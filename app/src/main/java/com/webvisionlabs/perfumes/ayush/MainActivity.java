package com.webvisionlabs.perfumes.ayush;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import androidx.annotation.Keep;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    WebView webview;
    RelativeLayout loaderEnclosure, content;
    SwipeRefreshLayout swipe;
    @SuppressLint("AddJavascriptInterface")

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        swipe=(SwipeRefreshLayout)findViewById(R.id.swipe);
        swipe.setOnRefreshListener((new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                LoadWeb();
            }
        }));
        LoadWeb();
    }

    public void LoadWeb(){
        if(!isConnected(MainActivity.this)) buildDialog(MainActivity.this).show();
        else {
            loaderEnclosure = findViewById(R.id.loaderEnclosure);
            content = findViewById(R.id.content);

            //[[Start WebView]]
            webview = findViewById(R.id.webview);
            swipe.setRefreshing(true);
            webview.setWebViewClient(new WebViewClient());

            webview.loadUrl("https://s9.ayushmarts.com");
            swipe.setRefreshing(false);
            webview.getSettings().setJavaScriptEnabled(true);
            webview.addJavascriptInterface(new WebAppInterface(this), "Android");

        }
    }

    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) return true;
            else return false;
        } else
            return false;
    }

    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You need to have Mobile Data or wifi to access this. Press ok to Exit");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();
            }
        });

        return builder;
    }

    @Override
    public void onBackPressed() {
        if(webview.canGoBack()) {
            webview.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Keep
    public class WebAppInterface {
        private Context context;

        /** Instantiate the interface and set the context */
        WebAppInterface(Context context){
            this.context = context;
        }

        /** Show a toast from the web page */
        @JavascriptInterface
        public void showToast(String toast) {
            Toast.makeText(context, toast, Toast.LENGTH_SHORT).show();
        }

        @JavascriptInterface
        public void call(String number){
            Intent i=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + number));
            context.startActivity(i);
        }

        @JavascriptInterface
        public void showLoader(){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loaderEnclosure.setVisibility(RelativeLayout.VISIBLE);
                    content.setVisibility(RelativeLayout.INVISIBLE);
                }
            });
        }

        @JavascriptInterface
        public void hideLoader(){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loaderEnclosure.setVisibility(RelativeLayout.INVISIBLE);
                    content.setVisibility(RelativeLayout.VISIBLE);
                }
            });
        }
    }

}
